#### Beijing housing market - exploratory analysis and price modeling

- [notebook preview](https://micstan.gitlab.io/share_zone/beijing_housing.nb.html)
- The notebook includes a short exploratory analysis and comparison of few selected models for house price prediction task.
- It uses Beijing housing prices from 2011-2017 period scrapped from Lianjia.com and shared on kaggle datastes: [https://www.kaggle.com/ruiqurm/lianjia](https://www.kaggle.com/ruiqurm/lianjia)
